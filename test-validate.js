const chai = require('chai');
const expect = chai.expect;
const validate = require('./validate');

describe('Validate Module', ()=> {
    context('Function isUserNameValid', ()=> {
        it('Function prototype : boolean isUserNameValid(username: String)', ()=> {
            expect(validate.isUserNameValid('kob')).to.be.true;
        })

        it('จำนวนตัวอักษรอย่างน้อย 3 ตัวอักษร', ()=> {
            expect(validate.isUserNameValid('tu')).to.be.false ;
        })

        it('ทุกตัวต้องเป็นตัวเล็ก', ()=> {
            expect(validate.isUserNameValid('Kob')).to.be.false ;
            expect(validate.isUserNameValid('koB')).to.be.false ;
        })

        it('จำนวนตัวอักษรที่มากที่สุดคือ 15 ตัวอักษร', ()=> {
            expect(validate.isUserNameValid('kob123456789012')).to.be.true ;
            expect(validate.isUserNameValid('kob1234567890123')).to.be.false ;
        })
    })

    context('function isAgeValid', ()=> {
        it('Function prototype : boolean isAgeValid (age: String)', ()=> {
            expect(validate.isAgeValid('18')).to.be.true;
        })
        
        it('age ต้องเป็นข้อความที่เป็นตัวเลข', ()=> {
            expect(validate.isAgeValid('a')).to.be.false;
        })

        it('อายุต้องไม่ต่ำกว่า 18 ปี และไม่เกิน 100 ปี', ()=> {
            expect(validate.isAgeValid('17')).to.be.false;
            expect(validate.isAgeValid('18')).to.be.true;
            expect(validate.isAgeValid('100')).to.be.true;
            expect(validate.isAgeValid('101')).to.be.false;
        })
    })

    context('function isPasswordValid', ()=> {
        it('Function prototype : boolean isUserNameValid(password: String)', ()=> {
            expect(validate.isPasswordValid('bbb')).to.be.false;
        })

        it('จำนวนตัวอักษรอย่างน้อย 8 ตัวอักษร', ()=> {
            expect(validate.isPasswordValid('ABC456%89')).to.be.true;
        })

        it('ต้องมีอักษรตัวใหญ่เป็นส่วนประกอบอย่างน้อย 1 ตัว', ()=> {
            expect(validate.isPasswordValid('B123#567')).to.be.true;
        })

        it('ต้องมีตัวเลขเป็นส่วนประกอบอย่างน้อย 3 ตัว', ()=> {
            expect(validate.isPasswordValid('B123@BCD')).to.be.true;
        })

        it('ต้องมีอักขระ พิเศษอย่างน้อย 1 ตัว', ()=> {
            expect(validate.isPasswordValid('B1234@CD')).to.be.true;
        })
    })

    context('function isDateValid', ()=> {
        it('Function prototype : boolean isDateValid(day: Integer, month: Integer, year: Integer)', ()=> {
            expect(validate.isDateValid(1,2,2019)).to.be.true;
        })

        it('day เริ่ม 1 และไม่เกิน 31 ในทุก ๆ เดือน', ()=> {
            expect(validate.isDateValid(32,1,2010)).to.be.false;
        })

        it('month เริ่มจาก 1 และไม่เกิน 12 ในทุก ๆ เดือน', ()=> {
            expect(validate.isDateValid(32,13,2010)).to.be.false;
            expect(validate.isDateValid(32,11,2010)).to.be.false;
        })

        it('year จะต้องไม่ต่ำกว่า 1970 และ ไม่เกิน ปี 2020', ()=> {
            expect(validate.isDateValid(3,12,2020)).to.be.true;
        })

        it('กรณีของปี อธิกสุรทิน ',()=> {
            expect(validate.isDateValid(29,2,1900)).to.be.false;
            expect(validate.isDateValid(29,2,2000)).to.be.true;
        })
    })
})