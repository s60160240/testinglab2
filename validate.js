
module.exports = {
    isUserNameValid: function(username){
        if(username.length<3 || username.length>15){
            return false;
        }
        //Kob -> kob
        if(username.toLowerCase() !== username){
            return false;
        }
        return true;    
    },

    isAgeValid: function(age){
        if(isNaN(age)){
            return false;
        }

        if(parseInt(age)<18 || parseInt(age)>100){
            return false;
        }
        return true;
    },

    isPasswordValid: function(password){
        if(password.length<8){
            return false;
        }

        const u = /[A-Z]/
        if(!u.test(password)){
            return false;
        }

        const digits = /[0-9]{3}/
        if(!digits.test(password)){
            return false;
        }

        const sc = /[`~!@#$%^&*()_|+\-=?;:'",.<>\{\}\[\]\\\/]/
        if(!sc.test(password)){
            return false;
        }
        return true;
    },

    isDateValid: function(day,month,year){
        if(month==1 || month==3 || month==5 || month==7 || month==8 || month==10 || month==12){
            if(day<1 || day > 31){
                return false;
            }
        }
        if(month==4 || month==6 || month==9 || month==11 ){
            if(day<1 || day>30){
                return false;
            }
        }
        if(month<1 || month>12){
            return false;
        }
        if(year<1970 || year>2020){
            return false;
        }
        if(month==2){
            if(year%400!==0){
                if(day<1 || day>28){
                    return false;
                }
            }
        }
        return true;
    }
}